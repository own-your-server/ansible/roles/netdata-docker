# Netdata docker

Tentative d'avoir de l'observabilité


## Disclaimer
/!\ l'a partie authentification est caprissieuse mais l'outil est superbe

L'authentification est une tentative avec keycloak mais ça ne reste pas stable longtemps. Il faut investiguer le problème.

C'est la raison de la dépendance à keycloak. Il est possible de modifier le docker-compose pour y accéder en direct.
## Installation

### dépendences:

nécessaire:
* [ansible docker host](https://framagit.org/cycles-manivelles/ansible-role_docker-host)

* [ansible traefik docker](https://framagit.org/cycles-manivelles/ansible-role_traefik-docker)

* [ansible keycloak docker](https://framagit.org/cycles-manivelles/ansible-role_keycloak-docker)
